# Custom Test Cases for AUD

## GrayCode Custom Tests
````java
@Test(timeout = 666)
public void customTestLength0() {
	BigBrother gcc = new BigBrother();
	String[] actual = GrayCode.generate(gcc, 0);
	System.out.println(actual.length);
	assertEquals("Unexpected length of returned Gray code array, should 0", 0, actual.length);
}
````

## Skyline Custom Test
````java
@Test(timeout = 666)
public void customTestAreaLowSkyline() {
	int[] skyline = { 1, 0, 3, 0, 9, 0, 12, 0, 16, 0, 19, 0, 22, 0, 23, 0, 29, 0 };
	int[] skylineClone = Arrays.copyOf(skyline, skyline.length);
	int expected = 0;
	int actual = SkylineSolver.area(skylineClone);
	//System.out.println(Arrays.toString(skyline)+" area: "+actual);
	assertEquals(SkylineSolverPublicTest.METHOD_NAME_area + "(" + Arrays.toString(skyline) + ") failed", expected, actual);
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_area + " - DO NOT MODIFY THE INPUT!", skyline, skylineClone);
}
````
````java
@Test(timeout = 666)
public void customTest_conquerLowSkyline() {
	SkylineSolverPublicTest.BigBrother ssh = new SkylineSolverPublicTest.BigBrother();
	
	int[][] buildings = new int[][] { { 2, 0, 7 }, { 3, 0, 9 }, { 12, 0, 16 }, { 14, 0, 25 }, { 19, 0, 22 }, { 23, 0, 29 }, { 24, 0, 28 } };
	int[] expected = { 2, 0 };
	int[] actual = SkylineSolver.conquer(ssh, buildings);
	//System.out.println("result: "+Arrays.toString(actual)+" expected: "+Arrays.toString(expected));
	assertArrayEquals(SkylineSolverPublicTest.METHOD_NAME_area + " - DO NOT MODIFY THE INPUT!", actual, expected);
}
````